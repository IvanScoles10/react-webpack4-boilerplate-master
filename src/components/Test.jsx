import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import logo from 'assets/images/logo.svg';

class Test extends Component {
    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        return (
            <div className="Home">
                <header className="Home-header">
                    <img src={logo} className="Home-logo" alt="logo" />
                    <h1 className="Home-title">REACT TEST PAGE</h1>
                </header>
                <p className="Home-intro">
                    To get started, edit <code>src/components/Test.js</code> and
                    save to reload.
                </p>
            </div>
        );
    }
}

export default withRouter(Test);
