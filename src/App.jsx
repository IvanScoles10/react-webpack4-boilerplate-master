import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import routes from 'routes';

const App = () => (
    <Router>
        <Switch>
            {routes.map(route => (
                <Route
                    key={route.name}
                    path={route.path}
                    exact={route.exact}
                    component={route.component}
                />
            ))}
        </Switch>
    </Router>
);

export default App;
