import Home from 'components/Home';
import Test from 'components/Test';

const routes = [
    {
        name: 'Home',
        path: '/',
        exact: true,
        component: Home,
    },
    {
        name: 'Test',
        path: '/test',
        component: Test,
    },
];

export default routes;
